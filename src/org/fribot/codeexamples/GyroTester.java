/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.codeexamples;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import static org.fribot.codeexamples.CodeExamples.GYRO_SENSOR;

/**
 *
 * @author jocelyn
 */
public class GyroTester {

    public static double getAngle() {
        SampleProvider sp = GYRO_SENSOR.getAngleMode();
        float[] samples = new float[sp.sampleSize()];
        
        sp.fetchSample(samples, 0);
        return samples[0];
    }

    public static void displayValues() {
        LCD.clear();
        GYRO_SENSOR.reset();
        while (!Button.ESCAPE.isDown()) {
            double angle = getAngle();
            LCD.drawString("Angle : ", 0, 0);
            LCD.drawString(angle+" °", 2, 1);;
            LCD.refresh();
            Delay.msDelay(100);
        }
        LCD.clear(); 
     }
}
