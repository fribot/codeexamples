/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.codeexamples;

import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.TextMenu;
import org.fribot.codeexamples.Behaviors.BackwardBehavior;
import org.fribot.codeexamples.Behaviors.ButtonPressedBehavior;
import org.fribot.codeexamples.Behaviors.ForwardBehavior;
import org.fribot.codeexamples.Behaviors.StopBehavior;

/**
 *
 * @author jocelyn
 */
public class CodeExamples {

    public static final EV3ColorSensor COLOR_SENSOR = new EV3ColorSensor(SensorPort.S1);
    public static final EV3UltrasonicSensor DISTANCE_SENSOR = new EV3UltrasonicSensor(SensorPort.S2);
    public static final EV3GyroSensor GYRO_SENSOR = new EV3GyroSensor(SensorPort.S3);
    public static final EV3LargeRegulatedMotor LEFT_MOTOR = new EV3LargeRegulatedMotor(MotorPort.A);
    public static final EV3LargeRegulatedMotor RIGHT_MOTOR = new EV3LargeRegulatedMotor(MotorPort.D);
    public static final DifferentialPilot PILOT = new DifferentialPilot(
        5.5, 5.5, 12.0, LEFT_MOTOR, RIGHT_MOTOR, true
    );
    public static boolean STOP_ARBITRATOR = false;

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TextMenu mainMenu = new TextMenu(new String[]{"ColorTester","DistanceTester","GyroTester"},1,"Main Menu");
        while(true) { 
            int selection = mainMenu.select();
            // Terminate program if ESCAPE it hit.
            if (selection<0) {
                break;
            }
            switch(selection){
                case 0:
                    while(true){
                        TextMenu colorMenu = new TextMenu(new String[]{"forward until black","Display values"});
                        int colorSelection = colorMenu.select();
                        if(colorSelection == 0) {
                            ColorTester.forwardUntilBlack();
                        } else if(colorSelection == 1){
                            ColorTester.displayValues();
                        } else if(colorSelection < 0) {
                            break;
                        }
                    } 
                    break;
                case 1:
                    while(true){
                        TextMenu distanceMenu = new TextMenu(new String[]{"forward until wall",
                            "Keep distance", "Display values"});
                        int distanceSelection = distanceMenu.select();
                        if(distanceSelection == 0) {
                            DistanceTester.forwardUntilWall(0.5);
                        } else if (distanceSelection == 1) {
                            PILOT.setAcceleration(20);
                            STOP_ARBITRATOR = false;
                            Behavior b1 = new BackwardBehavior();
                            Behavior b2 = new ForwardBehavior();
                            Behavior b3 = new StopBehavior();
                            Behavior b4 = new ButtonPressedBehavior();
                            Behavior[] behaviorList = {b1,b2,b3,b4};
                            Arbitrator arbitrator = new Arbitrator(behaviorList,true);
                            LCD.clear();
                            LCD.drawString("Press to exit...", 0, 2);
                            LCD.refresh();
                            arbitrator.start();
                        } else if(distanceSelection == 2){
                            DistanceTester.displayValues();
                        } else if(distanceSelection < 0) {
                            break;
                        }
                    } 
                    break;
                case 2:
                    GyroTester.displayValues();
                    break;
            }
        }
    }
    
}
