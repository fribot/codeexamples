/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.codeexamples;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import static org.fribot.codeexamples.CodeExamples.DISTANCE_SENSOR;
import static org.fribot.codeexamples.CodeExamples.PILOT;

/**
 *
 * @author jocelyn
 */
public class DistanceTester {
    
    public static double getDistance() {
        SampleProvider sp = DISTANCE_SENSOR.getDistanceMode();
        float[] samples = new float[sp.sampleSize()];
        
        sp.fetchSample(samples, 0);
        return samples[0];
    }

    public static void displayValues() {
        LCD.clear();
        while (!Button.ESCAPE.isDown()) {
            double distance = getDistance();
            LCD.drawString("Distance : ", 0, 0);
            LCD.drawString(distance+" m", 2, 1);
            LCD.refresh();
            Delay.msDelay(100);
        }
        LCD.clear(); 
     }
    
    public static void forwardUntilWall(double distance) {
        while(getDistance() > 0.5) {
            PILOT.forward();
        }
        PILOT.stop();
    }

}
