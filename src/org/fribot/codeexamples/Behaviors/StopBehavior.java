/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.codeexamples.Behaviors;

import lejos.robotics.subsumption.Behavior;
import static org.fribot.codeexamples.CodeExamples.PILOT;
import static org.fribot.codeexamples.CodeExamples.STOP_ARBITRATOR;
import org.fribot.codeexamples.DistanceTester;

/**
 *
 * @author jocelyn
 */
public class StopBehavior implements Behavior{

    @Override
    public boolean takeControl() {
        return (DistanceTester.getDistance() > 0.98 && DistanceTester.getDistance() < 1.02) && !STOP_ARBITRATOR;
    }

    @Override
    public void action() {
        PILOT.stop();
    }

    @Override
    public void suppress() {
    }
    
}
