/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.codeexamples.Behaviors;

import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;
import org.fribot.codeexamples.CodeExamples;
import static org.fribot.codeexamples.CodeExamples.PILOT;
import static org.fribot.codeexamples.CodeExamples.STOP_ARBITRATOR;
import org.fribot.codeexamples.DistanceTester;

/**
 *
 * @author jocelyn
 */
public class BackwardBehavior implements Behavior{

    @Override
    public boolean takeControl() {
        return DistanceTester.getDistance() <= 0.98 && !STOP_ARBITRATOR;
    }

    @Override
    public void action() {
        PILOT.backward();
    }

    @Override
    public void suppress() {
    }
    
}
