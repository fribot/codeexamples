/*
 *  Copyright Ivo Blöchliger, ivo.bloechliger@unifr.ch
 *  University of Fribourg.
 *  You may use and modify this code for teaching and learning 
 *  purposes. For any other use, please contact the author.
 */

package org.fribot.codeexamples.Behaviors;

import lejos.hardware.Button;
import lejos.robotics.subsumption.Behavior;
import static org.fribot.codeexamples.CodeExamples.STOP_ARBITRATOR;
import static org.fribot.codeexamples.CodeExamples.PILOT;



/**
 *
 * @author ivo
 */
public class ButtonPressedBehavior implements Behavior {

    boolean notYetPressed = true;
    
    @Override
    public boolean takeControl() {
        return Button.readButtons()!=0 && (!STOP_ARBITRATOR);
    }

    @Override
    public void action() {
        STOP_ARBITRATOR = true;
        PILOT.stop();
    }

    @Override
    public void suppress() {
    }
    
}
