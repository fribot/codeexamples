/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.codeexamples;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.sensor.SensorMode;
import lejos.utility.Delay;
import static org.fribot.codeexamples.CodeExamples.COLOR_SENSOR;
import static org.fribot.codeexamples.CodeExamples.PILOT;

/**
 *
 * @author jocelyn
 */
public class ColorTester {
        
    public static double getBrightness() {
        SensorMode sm = COLOR_SENSOR.getRedMode();
        float samples[] = new float[sm.sampleSize()];
        
        sm.fetchSample(samples, 0);
        return samples[0];
    }

    public static void displayValues() {           
        LCD.clear();
        while (!Button.ESCAPE.isDown()) {
            double brightness = getBrightness();
            LCD.drawString("Brightness : ", 0, 0);
            LCD.drawString(Double.toString(brightness), 2, 1);
            LCD.refresh();
            Delay.msDelay(100);
        }
        LCD.clear(); 
    }
    
    public static void forwardUntilBlack(){
        while(getBrightness() > 0.05) {
            PILOT.forward();
        }
        PILOT.stop();
    }
    
}
